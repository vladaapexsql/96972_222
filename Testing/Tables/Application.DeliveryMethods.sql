SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Application].[DeliveryMethods] (
		[DeliveryMethodID]       [int] NOT NULL,
		[DeliveryMethodName]     [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[LastEditedBy]           [int] NOT NULL,
		[ValidFrom]              [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
		[ValidTo]                [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
		PERIOD FOR SYSTEM_TIME ([ValidFrom], [ValidTo]),
		CONSTRAINT [UQ_Application_DeliveryMethods_DeliveryMethodName]
		UNIQUE
		NONCLUSTERED
		([DeliveryMethodName])
		ON [USERDATA],
		CONSTRAINT [PK_Application_DeliveryMethods]
		PRIMARY KEY
		CLUSTERED
		([DeliveryMethodID])
	ON [USERDATA]
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[Application].[DeliveryMethods_Archive]))
GO
ALTER TABLE [Application].[DeliveryMethods]
	ADD
	CONSTRAINT [DF_Application_DeliveryMethods_DeliveryMethodID]
	DEFAULT (NEXT VALUE FOR [Sequences].[DeliveryMethodID]) FOR [DeliveryMethodID]
GO
ALTER TABLE [Application].[DeliveryMethods]
	WITH CHECK
	ADD CONSTRAINT [FK_Application_DeliveryMethods_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Application].[DeliveryMethods]
	CHECK CONSTRAINT [FK_Application_DeliveryMethods_Application_People]

GO
EXEC sp_addextendedproperty N'Description', N'Ways that stock items can be delivered (ie: truck/van, post, pickup, courier, etc.', 'SCHEMA', N'Application', 'TABLE', N'DeliveryMethods', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a delivery method within the database', 'SCHEMA', N'Application', 'TABLE', N'DeliveryMethods', 'COLUMN', N'DeliveryMethodID'
GO
EXEC sp_addextendedproperty N'Description', N'Full name of methods that can be used for delivery of customer orders', 'SCHEMA', N'Application', 'TABLE', N'DeliveryMethods', 'COLUMN', N'DeliveryMethodName'
GO
ALTER TABLE [Application].[DeliveryMethods] SET (LOCK_ESCALATION = TABLE)
GO
