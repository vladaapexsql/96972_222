SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Purchasing].[PurchaseOrders] (
		[PurchaseOrderID]          [int] NOT NULL,
		[SupplierID]               [int] NOT NULL,
		[OrderDate]                [date] NOT NULL,
		[DeliveryMethodID]         [int] NOT NULL,
		[ContactPersonID]          [int] NOT NULL,
		[ExpectedDeliveryDate]     [date] NULL,
		[SupplierReference]        [nvarchar](20) COLLATE Latin1_General_100_CI_AS NULL,
		[IsOrderFinalized]         [bit] NOT NULL,
		[Comments]                 [nvarchar](max) COLLATE Latin1_General_100_CI_AS NULL,
		[InternalComments]         [nvarchar](max) COLLATE Latin1_General_100_CI_AS NULL,
		[LastEditedBy]             [int] NOT NULL,
		[LastEditedWhen]           [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Purchasing_PurchaseOrders]
		PRIMARY KEY
		CLUSTERED
		([PurchaseOrderID])
	ON [USERDATA]
)
GO
ALTER TABLE [Purchasing].[PurchaseOrders]
	ADD
	CONSTRAINT [DF_Purchasing_PurchaseOrders_PurchaseOrderID]
	DEFAULT (NEXT VALUE FOR [Sequences].[PurchaseOrderID]) FOR [PurchaseOrderID]
GO
ALTER TABLE [Purchasing].[PurchaseOrders]
	ADD
	CONSTRAINT [DF_Purchasing_PurchaseOrders_LastEditedWhen]
	DEFAULT (sysdatetime()) FOR [LastEditedWhen]
GO
ALTER TABLE [Purchasing].[PurchaseOrders]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_PurchaseOrders_SupplierID_Purchasing_Suppliers]
	FOREIGN KEY ([SupplierID]) REFERENCES [Purchasing].[Suppliers] ([SupplierID])
ALTER TABLE [Purchasing].[PurchaseOrders]
	CHECK CONSTRAINT [FK_Purchasing_PurchaseOrders_SupplierID_Purchasing_Suppliers]

GO
ALTER TABLE [Purchasing].[PurchaseOrders]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_PurchaseOrders_DeliveryMethodID_Application_DeliveryMethods]
	FOREIGN KEY ([DeliveryMethodID]) REFERENCES [Application].[DeliveryMethods] ([DeliveryMethodID])
ALTER TABLE [Purchasing].[PurchaseOrders]
	CHECK CONSTRAINT [FK_Purchasing_PurchaseOrders_DeliveryMethodID_Application_DeliveryMethods]

GO
ALTER TABLE [Purchasing].[PurchaseOrders]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_PurchaseOrders_ContactPersonID_Application_People]
	FOREIGN KEY ([ContactPersonID]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Purchasing].[PurchaseOrders]
	CHECK CONSTRAINT [FK_Purchasing_PurchaseOrders_ContactPersonID_Application_People]

GO
ALTER TABLE [Purchasing].[PurchaseOrders]
	WITH CHECK
	ADD CONSTRAINT [FK_Purchasing_PurchaseOrders_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Purchasing].[PurchaseOrders]
	CHECK CONSTRAINT [FK_Purchasing_PurchaseOrders_Application_People]

GO
CREATE NONCLUSTERED INDEX [FK_Purchasing_PurchaseOrders_SupplierID]
	ON [Purchasing].[PurchaseOrders] ([SupplierID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'INDEX', N'FK_Purchasing_PurchaseOrders_SupplierID'
GO
CREATE NONCLUSTERED INDEX [FK_Purchasing_PurchaseOrders_DeliveryMethodID]
	ON [Purchasing].[PurchaseOrders] ([DeliveryMethodID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'INDEX', N'FK_Purchasing_PurchaseOrders_DeliveryMethodID'
GO
CREATE NONCLUSTERED INDEX [FK_Purchasing_PurchaseOrders_ContactPersonID]
	ON [Purchasing].[PurchaseOrders] ([ContactPersonID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'INDEX', N'FK_Purchasing_PurchaseOrders_ContactPersonID'
GO
EXEC sp_addextendedproperty N'Description', N'Details of supplier purchase orders', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a purchase order within the database', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'COLUMN', N'PurchaseOrderID'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier for this purchase order', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'COLUMN', N'SupplierID'
GO
EXEC sp_addextendedproperty N'Description', N'Date that this purchase order was raised', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'COLUMN', N'OrderDate'
GO
EXEC sp_addextendedproperty N'Description', N'How this purchase order should be delivered', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'COLUMN', N'DeliveryMethodID'
GO
EXEC sp_addextendedproperty N'Description', N'The person who is the primary contact for this purchase order', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'COLUMN', N'ContactPersonID'
GO
EXEC sp_addextendedproperty N'Description', N'Expected delivery date for this purchase order', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'COLUMN', N'ExpectedDeliveryDate'
GO
EXEC sp_addextendedproperty N'Description', N'Supplier reference for our organization (might be our account number at the supplier)', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'COLUMN', N'SupplierReference'
GO
EXEC sp_addextendedproperty N'Description', N'Is this purchase order now considered finalized?', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'COLUMN', N'IsOrderFinalized'
GO
EXEC sp_addextendedproperty N'Description', N'Any comments related this purchase order (comments sent to the supplier)', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'COLUMN', N'Comments'
GO
EXEC sp_addextendedproperty N'Description', N'Any internal comments related this purchase order (comments for internal reference only and not sent to the supplier)', 'SCHEMA', N'Purchasing', 'TABLE', N'PurchaseOrders', 'COLUMN', N'InternalComments'
GO
ALTER TABLE [Purchasing].[PurchaseOrders] SET (LOCK_ESCALATION = TABLE)
GO
