SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Sales].[CustomerCategories] (
		[CustomerCategoryID]       [int] NOT NULL,
		[CustomerCategoryName]     [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[LastEditedBy]             [int] NOT NULL,
		[ValidFrom]                [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
		[ValidTo]                  [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
		PERIOD FOR SYSTEM_TIME ([ValidFrom], [ValidTo]),
		CONSTRAINT [UQ_Sales_CustomerCategories_CustomerCategoryName]
		UNIQUE
		NONCLUSTERED
		([CustomerCategoryName])
		ON [USERDATA],
		CONSTRAINT [PK_Sales_CustomerCategories]
		PRIMARY KEY
		CLUSTERED
		([CustomerCategoryID])
	ON [USERDATA]
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[Sales].[CustomerCategories_Archive]))
GO
ALTER TABLE [Sales].[CustomerCategories]
	ADD
	CONSTRAINT [DF_Sales_CustomerCategories_CustomerCategoryID]
	DEFAULT (NEXT VALUE FOR [Sequences].[CustomerCategoryID]) FOR [CustomerCategoryID]
GO
ALTER TABLE [Sales].[CustomerCategories]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_CustomerCategories_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Sales].[CustomerCategories]
	CHECK CONSTRAINT [FK_Sales_CustomerCategories_Application_People]

GO
EXEC sp_addextendedproperty N'Description', N'Categories for customers (ie restaurants, cafes, supermarkets, etc.)', 'SCHEMA', N'Sales', 'TABLE', N'CustomerCategories', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a customer category within the database', 'SCHEMA', N'Sales', 'TABLE', N'CustomerCategories', 'COLUMN', N'CustomerCategoryID'
GO
EXEC sp_addextendedproperty N'Description', N'Full name of the category that customers can be assigned to', 'SCHEMA', N'Sales', 'TABLE', N'CustomerCategories', 'COLUMN', N'CustomerCategoryName'
GO
ALTER TABLE [Sales].[CustomerCategories] SET (LOCK_ESCALATION = TABLE)
GO
