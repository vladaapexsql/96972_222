SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Application].[SystemParameters] (
		[SystemParameterID]        [int] NOT NULL,
		[DeliveryAddressLine1]     [nvarchar](60) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[DeliveryAddressLine2]     [nvarchar](60) COLLATE Latin1_General_100_CI_AS NULL,
		[DeliveryCityID]           [int] NOT NULL,
		[DeliveryPostalCode]       [nvarchar](10) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[DeliveryLocation]         [geography] NOT NULL,
		[PostalAddressLine1]       [nvarchar](60) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[PostalAddressLine2]       [nvarchar](60) COLLATE Latin1_General_100_CI_AS NULL,
		[PostalCityID]             [int] NOT NULL,
		[PostalPostalCode]         [nvarchar](10) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[ApplicationSettings]      [nvarchar](max) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[LastEditedBy]             [int] NOT NULL,
		[LastEditedWhen]           [datetime2](7) NOT NULL,
		CONSTRAINT [PK_Application_SystemParameters]
		PRIMARY KEY
		CLUSTERED
		([SystemParameterID])
	ON [USERDATA]
)
GO
ALTER TABLE [Application].[SystemParameters]
	ADD
	CONSTRAINT [DF_Application_SystemParameters_SystemParameterID]
	DEFAULT (NEXT VALUE FOR [Sequences].[SystemParameterID]) FOR [SystemParameterID]
GO
ALTER TABLE [Application].[SystemParameters]
	ADD
	CONSTRAINT [DF_Application_SystemParameters_LastEditedWhen]
	DEFAULT (sysdatetime()) FOR [LastEditedWhen]
GO
ALTER TABLE [Application].[SystemParameters]
	WITH CHECK
	ADD CONSTRAINT [FK_Application_SystemParameters_DeliveryCityID_Application_Cities]
	FOREIGN KEY ([DeliveryCityID]) REFERENCES [Application].[Cities] ([CityID])
ALTER TABLE [Application].[SystemParameters]
	CHECK CONSTRAINT [FK_Application_SystemParameters_DeliveryCityID_Application_Cities]

GO
ALTER TABLE [Application].[SystemParameters]
	WITH CHECK
	ADD CONSTRAINT [FK_Application_SystemParameters_PostalCityID_Application_Cities]
	FOREIGN KEY ([PostalCityID]) REFERENCES [Application].[Cities] ([CityID])
ALTER TABLE [Application].[SystemParameters]
	CHECK CONSTRAINT [FK_Application_SystemParameters_PostalCityID_Application_Cities]

GO
ALTER TABLE [Application].[SystemParameters]
	WITH CHECK
	ADD CONSTRAINT [FK_Application_SystemParameters_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Application].[SystemParameters]
	CHECK CONSTRAINT [FK_Application_SystemParameters_Application_People]

GO
CREATE NONCLUSTERED INDEX [FK_Application_SystemParameters_DeliveryCityID]
	ON [Application].[SystemParameters] ([DeliveryCityID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'INDEX', N'FK_Application_SystemParameters_DeliveryCityID'
GO
CREATE NONCLUSTERED INDEX [FK_Application_SystemParameters_PostalCityID]
	ON [Application].[SystemParameters] ([PostalCityID])
	ON [USERDATA]
GO
EXEC sp_addextendedproperty N'Description', N'Auto-created to support a foreign key', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'INDEX', N'FK_Application_SystemParameters_PostalCityID'
GO
EXEC sp_addextendedproperty N'Description', N'Any configurable parameters for the whole system', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for row holding system parameters', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'COLUMN', N'SystemParameterID'
GO
EXEC sp_addextendedproperty N'Description', N'First address line for the company', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'COLUMN', N'DeliveryAddressLine1'
GO
EXEC sp_addextendedproperty N'Description', N'Second address line for the company', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'COLUMN', N'DeliveryAddressLine2'
GO
EXEC sp_addextendedproperty N'Description', N'ID of the city for this address', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'COLUMN', N'DeliveryCityID'
GO
EXEC sp_addextendedproperty N'Description', N'Postal code for the company', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'COLUMN', N'DeliveryPostalCode'
GO
EXEC sp_addextendedproperty N'Description', N'Geographic location for the company office', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'COLUMN', N'DeliveryLocation'
GO
EXEC sp_addextendedproperty N'Description', N'First postal address line for the company', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'COLUMN', N'PostalAddressLine1'
GO
EXEC sp_addextendedproperty N'Description', N'Second postaladdress line for the company', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'COLUMN', N'PostalAddressLine2'
GO
EXEC sp_addextendedproperty N'Description', N'ID of the city for this postaladdress', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'COLUMN', N'PostalCityID'
GO
EXEC sp_addextendedproperty N'Description', N'Postal code for the company when sending via mail', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'COLUMN', N'PostalPostalCode'
GO
EXEC sp_addextendedproperty N'Description', N'JSON-structured application settings', 'SCHEMA', N'Application', 'TABLE', N'SystemParameters', 'COLUMN', N'ApplicationSettings'
GO
ALTER TABLE [Application].[SystemParameters] SET (LOCK_ESCALATION = TABLE)
GO
