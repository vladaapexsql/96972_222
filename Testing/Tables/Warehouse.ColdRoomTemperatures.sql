SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Warehouse].[ColdRoomTemperatures] (
		[ColdRoomTemperatureID]     [bigint] IDENTITY(1, 1) NOT NULL,
		[ColdRoomSensorNumber]      [int] NOT NULL,
		[RecordedWhen]              [datetime2](7) NOT NULL,
		[Temperature]               [decimal](10, 2) NOT NULL,
		[ValidFrom]                 [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
		[ValidTo]                   [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
		PERIOD FOR SYSTEM_TIME ([ValidFrom], [ValidTo]),
		CONSTRAINT [PK_Warehouse_ColdRoomTemperatures]
		PRIMARY KEY
		NONCLUSTERED
		([ColdRoomTemperatureID])
)
WITH (MEMORY_OPTIMIZED=ON, DURABILITY=SCHEMA_AND_DATA)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[Warehouse].[ColdRoomTemperatures_Archive]))
GO
ALTER TABLE [Warehouse].[ColdRoomTemperatures]
	ADD
	INDEX [IX_Warehouse_ColdRoomTemperatures_ColdRoomSensorNumber] ([ColdRoomSensorNumber])
