SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Application].[PaymentMethods] (
		[PaymentMethodID]       [int] NOT NULL,
		[PaymentMethodName]     [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[LastEditedBy]          [int] NOT NULL,
		[ValidFrom]             [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
		[ValidTo]               [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
		PERIOD FOR SYSTEM_TIME ([ValidFrom], [ValidTo]),
		CONSTRAINT [UQ_Application_PaymentMethods_PaymentMethodName]
		UNIQUE
		NONCLUSTERED
		([PaymentMethodName])
		ON [USERDATA],
		CONSTRAINT [PK_Application_PaymentMethods]
		PRIMARY KEY
		CLUSTERED
		([PaymentMethodID])
	ON [USERDATA]
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[Application].[PaymentMethods_Archive]))
GO
ALTER TABLE [Application].[PaymentMethods]
	ADD
	CONSTRAINT [DF_Application_PaymentMethods_PaymentMethodID]
	DEFAULT (NEXT VALUE FOR [Sequences].[PaymentMethodID]) FOR [PaymentMethodID]
GO
ALTER TABLE [Application].[PaymentMethods]
	WITH CHECK
	ADD CONSTRAINT [FK_Application_PaymentMethods_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Application].[PaymentMethods]
	CHECK CONSTRAINT [FK_Application_PaymentMethods_Application_People]

GO
EXEC sp_addextendedproperty N'Description', N'Ways that payments can be made (ie: cash, check, EFT, etc.', 'SCHEMA', N'Application', 'TABLE', N'PaymentMethods', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a payment type within the database', 'SCHEMA', N'Application', 'TABLE', N'PaymentMethods', 'COLUMN', N'PaymentMethodID'
GO
EXEC sp_addextendedproperty N'Description', N'Full name of ways that customers can make payments or that suppliers can be paid', 'SCHEMA', N'Application', 'TABLE', N'PaymentMethods', 'COLUMN', N'PaymentMethodName'
GO
ALTER TABLE [Application].[PaymentMethods] SET (LOCK_ESCALATION = TABLE)
GO
