SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [Sales].[BuyingGroups] (
		[BuyingGroupID]       [int] NOT NULL,
		[BuyingGroupName]     [nvarchar](50) COLLATE Latin1_General_100_CI_AS NOT NULL,
		[LastEditedBy]        [int] NOT NULL,
		[ValidFrom]           [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
		[ValidTo]             [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
		PERIOD FOR SYSTEM_TIME ([ValidFrom], [ValidTo]),
		CONSTRAINT [UQ_Sales_BuyingGroups_BuyingGroupName]
		UNIQUE
		NONCLUSTERED
		([BuyingGroupName])
		ON [USERDATA],
		CONSTRAINT [PK_Sales_BuyingGroups]
		PRIMARY KEY
		CLUSTERED
		([BuyingGroupID])
	ON [USERDATA]
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[Sales].[BuyingGroups_Archive]))
GO
ALTER TABLE [Sales].[BuyingGroups]
	ADD
	CONSTRAINT [DF_Sales_BuyingGroups_BuyingGroupID]
	DEFAULT (NEXT VALUE FOR [Sequences].[BuyingGroupID]) FOR [BuyingGroupID]
GO
ALTER TABLE [Sales].[BuyingGroups]
	WITH CHECK
	ADD CONSTRAINT [FK_Sales_BuyingGroups_Application_People]
	FOREIGN KEY ([LastEditedBy]) REFERENCES [Application].[People] ([PersonID])
ALTER TABLE [Sales].[BuyingGroups]
	CHECK CONSTRAINT [FK_Sales_BuyingGroups_Application_People]

GO
EXEC sp_addextendedproperty N'Description', N'Customer organizations can be part of groups that exert greater buying power', 'SCHEMA', N'Sales', 'TABLE', N'BuyingGroups', NULL, NULL
GO
EXEC sp_addextendedproperty N'Description', N'Numeric ID used for reference to a buying group within the database', 'SCHEMA', N'Sales', 'TABLE', N'BuyingGroups', 'COLUMN', N'BuyingGroupID'
GO
EXEC sp_addextendedproperty N'Description', N'Full name of a buying group that customers can be members of', 'SCHEMA', N'Sales', 'TABLE', N'BuyingGroups', 'COLUMN', N'BuyingGroupName'
GO
ALTER TABLE [Sales].[BuyingGroups] SET (LOCK_ESCALATION = TABLE)
GO
